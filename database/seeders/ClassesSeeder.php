<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
	public function run()
	{
		DB::table('school_classes')->delete();

		DB::table('school_classes')->insert(
			[
				'name' => 'Math',
			]
		);
		DB::table('school_classes')->insert(
			[
				'name' => 'Science',
			]
		);

		DB::table('school_classes')->insert(
			[
				'name' => 'Art',
			]
		);

	}
}
