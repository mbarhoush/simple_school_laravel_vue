<?php


namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->where('email', 'admin@gmail.com')->delete();

	    DB::table('roles')->insert([
	    	'id'    =>  1,
		    'name'  =>  'Admin'
	    ]);
	    DB::table('roles')->insert([
	    	'id'    =>  2,
		    'name'  =>  'Teacher'
	    ]);
	    DB::table('roles')->insert([
	    	'id'    =>  3,
		    'name'  =>  'Student'
	    ]);


	    DB::table('users')->insert(
        	[
        		'id'    =>  1,
	            'name' => 'John Doe',
	            'email' => 'admin@gmail.com',
	            'password' => bcrypt('123456'),
	            'type' => 'admin'
            ]
        );
        DB::table('users')->insert(
	        [
	        	'id'    =>  2,
		        'name' => 'Mr. John',
		        'email' => 'teacher@gmail.com',
		        'password' => bcrypt('123456'),
		        'type' => 'teacher',
	        ]
        );
        DB::table('users')->insert(
	        [
	        	'id'   =>   3,
		        'name' => 'Doe',
		        'email' => 'student@gmail.com',
		        'password' => bcrypt('123456'),
		        'type' => 'student',
	        ]
        );

        DB::table('role_user')->insert([
            'role_id'   =>  1,
	        'user_id'   =>  1,
        ]);
        DB::table('role_user')->insert([
            'role_id'   =>  2,
	        'user_id'   =>  2,
        ]);
        DB::table('role_user')->insert([
            'role_id'   =>  3,
	        'user_id'   =>  3,
        ]);



    }
}
