export default [
    { path: '/dashboard', component: require('./components/Dashboard.vue').default },
    { path: '/profile', component: require('./components/Profile.vue').default },
    { path: '/users', component: require('./components/Users.vue').default },
    { path: '/classes', component: require('./components/Classes.vue').default },
    { path: '/user_classes/:id', component: require('./components/UsersClasses.vue').default, props: true, params: true },
    { path: '/user_marks/:user_id/:class_id', component: require('./components/UsersClasses.vue').default, props: true, params: true },
    { path: '*', component: require('./components/NotFound.vue').default }
];
