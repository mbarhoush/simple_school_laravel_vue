<?php

namespace App\Http\Controllers;

use App\Models\SchoolClass;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function test(){
    	$user   =   User::find(5);

    	$class  =   SchoolClass::find(4);
    	$id =   4;
	    $otherUsers = User::whereDoesntHave('classes', function($q) use ($id){
		    $q->where('class_id', $id);
	    })->toSql();
    	dd($otherUsers);
//    	dd($user->classes()->get());
    }
}
