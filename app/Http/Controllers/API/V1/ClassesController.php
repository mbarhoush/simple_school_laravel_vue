<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\Classes\SchoolClassRequest;
use App\Models\SchoolClass;
use Illuminate\Support\Facades\Hash;

class ClassesController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!\Gate::allows('isAdmin')) {
            return $this->unauthorizedResponse();
        }
        // $this->authorize('isAdmin');

        $records = SchoolClass::latest()->paginate(10);

        return $this->sendResponse($records, 'Classes list');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Classes\SchoolClassRequest  $request
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(SchoolClassRequest $request)
    {
	    $record = SchoolClass::create([
            'name' => $request['name'],
        ]);

        return $this->sendResponse($record, 'Record Created Successfully');
    }

    /**
     * Update the resource in storage
     *
     * @param  \App\Http\Requests\Classes\SchoolClassRequest  $request
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */

	public function show($id)
	{
		dd('gotcha');
	}

    public function update(SchoolClassRequest $request, $id)
    {
        $record = SchoolClass::findOrFail($id);
        if (!empty($request->password)) {
            $request->merge(['password' => Hash::make($request['password'])]);
        }

	    $record->update($request->all());

        return $this->sendResponse($record, 'Record Information has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->authorize('isAdmin');

        $record = SchoolClass::findOrFail($id);
        // delete the user

        $record->delete();

        return $this->sendResponse([$record], 'Record has been Deleted');
    }

    public function users(){
    	dd('gotcha');
    }
}
