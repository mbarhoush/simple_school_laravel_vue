<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\Classes\StudentClassRequest;
use App\Models\SchoolClass;
use App\Models\StudentClass;
use App\Models\User;
use Illuminate\Http\Client\Request;

class UsersMarksController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }


    public function show($id)
    {
	    if (!\Gate::allows('isAdmin')) {
		    return $this->unauthorizedResponse();
	    }
	    // $this->authorize('isAdmin');

	    $class  =   SchoolClass::find($id);
	    $records    =   $class->users()->latest()->paginate(10);
	    $otherUsers = User::whereDoesntHave('classes', function($q) use ($id){
		    $q->where('class_id', $id);
	    })->get();
	    $data   =   [
	        'class_users'   =>  	$records,
		    'other_users'   =>  $otherUsers
	    ];
	    return $this->sendResponse($data, 'Users Classes list');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Classes\StudentClassRequest  $request
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StudentClassRequest $request)
    {
    	$newUsers   =   $request['newUsers'];
    	$classModel =   SchoolClass::findOrFail($request->query('class'));
    	foreach ($newUsers  as  $user){
    		$userModel  =   User::find($user);
    		if($userModel){
			    StudentClass::create([
				    'class_id' => $classModel->id,
				    'user_id' => $userModel->id,
				    'user_type' => $userModel->type,
			    ]);
    		}

	    }


        return $this->sendResponse(true, 'Record Created Successfully');
    }

    /**
     * Update the resource in storage
     *
     * @param  \App\Http\Requests\Classes\StudentClassRequest  $request
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(StudentClassRequest $request, $id)
    {
    	dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->authorize('isAdmin');

        $record = StudentClass::findOrFail($id);
        // delete the user

        $record->delete();

        return $this->sendResponse([$record], 'Record has been Deleted');
    }
}
