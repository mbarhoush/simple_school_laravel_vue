<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model //This should be named Class, but it's reserved word in php
{
    use HasFactory;
    protected $table    =   'school_classes';
    protected $fillable =   ['name'];

	public function users(){
		return $this->belongsToMany(User::class,    'student_classes',  'class_id',  'user_id');
	}
}
