<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentClass extends Model    //The pivot table between Users -> SchoolClass
{
    use HasFactory;
    protected $table    =   'student_classes';
    protected $fillable =   ['class_id',    'user_id',  'user_type'];
}
