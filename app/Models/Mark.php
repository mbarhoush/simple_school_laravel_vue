<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    use HasFactory;
    protected $table    =   'marks';
    protected $fillable =   ['name',    'class_id', 'user_id',  'mark'];

    public function user(){
	    return $this->belongsTo(User::class);
    }
}
